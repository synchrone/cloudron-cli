/* jshint node:true */

'use strict';

var assert = require('assert'),
    async = require('async'),
    config = require('./config.js'),
    debug = require('debug')('cloudron:helper'),
    fs = require('fs'),
    ipaddr = require('ipaddr.js'),
    path = require('path'),
    os = require('os'),
    readlineSync = require('readline-sync'),
    safe = require('safetydance'),
    semver = require('semver'),
    SshClient = require('ssh2').Client,
    spawn = require('child_process').spawn,
    superagent = require('superagent'),
    util = require('util');

exports = module.exports = {
    exit: exit,
    missing: missing,

    // fix int parsing for commander on newer node versions,
    parseInteger: function (input, fallback) { return parseInt(input, 10) || fallback; },

    locateManifest: locateManifest,
    getAppstoreId: getAppstoreId,
    verifyArguments: verifyArguments,

    setBuildConfig: setBuildConfig,
    getBuildConfig: getBuildConfig,

    addBuild: addBuild,
    updateBuild: updateBuild,
    selectImage: selectImage,
    selectBuild: selectBuild,
    selectUserSync: selectUserSync,

    showDeveloperModeNotice: showDeveloperModeNotice,
    detectCloudronApiEndpoint: detectCloudronApiEndpoint,
    isIp: isIp,

    exec: exec,
    getSSH: getSSH,
    findSSHKey: findSSHKey,
    getSSHFingerprint: getSSHFingerprint,
    getSSHPublicKey: getSSHPublicKey,
    sshExec: sshExec,

    createUrl: createUrl,
    authenticate: authenticate,
    superagentEnd: superagentEnd,

    getDomains: getDomains,

    // these work with the rest route
    waitForBackupFinish: waitForBackupFinish,
    waitForUpdateFinish: waitForUpdateFinish,
    getCloudronBackupList: getCloudronBackupList,
    createCloudronBackup: createCloudronBackup,
    updateCloudron: updateCloudron
};

function exit(error) {
    if (error instanceof Error) console.log(error.message.red);
    else if (error) console.error(util.format.apply(null, Array.prototype.slice.call(arguments)).red);

    process.exit(error ? 1 : 0);
}

function missing(argument) {
    exit('You must specify --' + argument);
}

function locateManifest() {
    var curdir = process.cwd();
    do {
        var candidate = path.join(curdir, 'CloudronManifest.json');
        if (fs.existsSync(candidate)) return candidate;

        // check if we can't go further up (the previous check for '/' breaks on windows)
        if (curdir === path.resolve(curdir, '..')) break;

        curdir = path.resolve(curdir, '..');
    } while (true);

    return null;
}

// the app argument allows us in the future to get by name or id
function getAppstoreId(appstoreId, callback) {
    if (appstoreId) {
        var parts = appstoreId.split('@');

        return callback(null, parts[0], parts[1]);
    }

    var manifestFilePath = locateManifest();

    if (!manifestFilePath) return callback('No CloudronManifest.json found');

    var manifest = safe.JSON.parse(safe.fs.readFileSync(manifestFilePath));
    if (!manifest) callback(util.format('Unable to read manifest %s. Error: %s', manifestFilePath, safe.error));

    return callback(null, manifest.id, manifest.version);
}

function verifyArguments(args) {
    if (args.length > 1) {
        console.log('Too many arguments');
        args[args.length-1].parent.help();
        process.exit(1);
    }
}

function prettyDate(time) {
    var date = new Date(time),
        diff = (((new Date()).getTime() - date.getTime()) / 1000),
        day_diff = Math.floor(diff / 86400);

    if (isNaN(day_diff) || day_diff < 0 || day_diff >= 31)
        return;

    return day_diff === 0 && (
            diff < 60 && 'just now' ||
            diff < 120 && '1 minute ago' ||
            diff < 3600 && Math.floor( diff / 60 ) + ' minutes ago' ||
            diff < 7200 && '1 hour ago' ||
            diff < 86400 && Math.floor( diff / 3600 ) + ' hours ago') ||
        day_diff == 1 && 'Yesterday' ||
        day_diff < 7 && day_diff + ' days ago' ||
        day_diff < 31 && Math.ceil( day_diff / 7 ) + ' weeks ago';
}

function setBuildConfig(appId, buildConfig) {
    var appConfig = config.get('apps.' + appId) || {};
    appConfig.buildConfig = buildConfig;
    config.set('apps.' + appId, appConfig);
}

function getBuildConfig(appId) {
    var appConfig = config.get('apps.' + appId) || {};
    return appConfig.buildConfig || {};
}

function addBuild(appId, buildId) {
    var appConfig = config.get('apps.' + appId) || {};
    if (!appConfig.builds) appConfig.builds = [];
    appConfig.builds.push({ id: buildId, ts: new Date().toISOString() });
    config.set('apps.' + appId, appConfig);
}

function updateBuild(appId, buildId, dockerImage) {
    var appConfig = config.get('apps.' + appId);
    if (!appConfig.builds) appConfig.builds = [];
    appConfig.builds.forEach(function (build) { if (build.id === buildId) build.dockerImage = dockerImage; });
    config.set('apps.' + appId, appConfig);
}

function selectImage(manifest, image, latest, callback) {
    assert(typeof manifest === 'object');
    assert(typeof image === 'string');
    assert(typeof latest === 'boolean');
    assert(typeof callback === 'function');

    if (image) return callback(null, image); // user specified image

    selectBuild(manifest.id, latest, function (error, build) {
        if (error) return callback(error);
        return callback(null, build.dockerImage);
    });
}

function selectBuild(appId, latest, callback) {
    assert(typeof appId === 'string');
    assert(typeof latest === 'boolean');
    assert(typeof callback === 'function');

    var appConfig = config.get('apps.' + appId) || {};
    if (!appConfig.builds) appConfig.builds = [];
    let builds = appConfig.builds;

    if (builds.length === 0) return callback(new Error('No build found'));

    // builds are sorted by time already
    if (builds.length === 1 || latest) {
        var build = builds[builds.length - 1];
        return callback(null, build);
    }

    console.log();
    console.log('Available builds:');
    builds.forEach(function (build, index) {
        console.log('[%s]\t%s - %s', index, build.id.cyan, prettyDate(build.ts).bold);
    });

    var index = -1;
    while (true) {
        index = parseInt(readlineSync.question('Choose build [0-' + (builds.length-1) + ']: ', {}));
        if (isNaN(index) || index < 0 || index > builds.length-1) console.log('Invalid selection'.red);
        else break;
    }

    console.log();

    callback(null, builds[index]);
}

function selectUserSync(users) {
    assert(typeof users === 'object');

    if (users.length === 1) return users[0];

    console.log();
    console.log('Available users:');
    users.forEach(function (user, index) {
        console.log('[%s]\t%s - %s', index, (user.username || '(unset)').cyan, user.email);
    });

    var index = -1;
    while (true) {
        index = parseInt(readlineSync.question('Choose user [0-' + (users.length-1) + ']: ', {}));
        if (isNaN(index) || index < 0 || index > users.length-1) console.log('Invalid selection'.red);
        else break;
    }

    console.log();

    return users[index];
}

function showDeveloperModeNotice(endpoint) {
    assert(typeof endpoint === 'string');

    console.error('CLI mode is disabled. Enable it at %s.'.red, 'https://' + endpoint + '/#/settings');
}

function detectCloudronApiEndpoint(cloudron, callback) {
    assert(typeof cloudron === 'string');
    assert(typeof callback === 'function');

    if (cloudron.indexOf('https://') === 0) cloudron = cloudron.slice('https://'.length);
    if (cloudron.indexOf('/') !== -1) cloudron = cloudron.slice(0, cloudron.indexOf('/'));

    // check for selfsigned value
    if (config.get('cloudrons', cloudron + '.allowSelfsigned')) process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';

    superagent.get('https://' + cloudron + '/api/v1/cloudron/status').timeout(10000).end(function (error, result) {
        if (!error && result.statusCode === 200) {
            if (!result.body.provider) console.log('WARNING provider is not set, this is most likely a bug! Falling back to caas.'.red);

            config.setActive(cloudron);
            config.setApiEndpoint(cloudron);
            config.setProvider(result.body.provider || 'caas');
            config.setVersion(result.body.version);

            return callback(null);
        }

        callback(util.format('Cloudron %s not found.\nTry providing the admin location, probably my.%s', cloudron.bold, cloudron));
    });
}

function isIp(input) {
    assert(typeof input === 'string');

    // accepts both ipv4 and ipv6
    return ipaddr.isValid(input);
}

// do not pipe fds. otherwise, the shell does not detect input as a tty and does not change the terminal window size
// https://groups.google.com/forum/#!topic/nodejs/vxIwmRdhrWE
function exec(command, args, callback) {
    var options = {};

    function spawnNow() {
        var child = spawn(command, args, options);

        callback = callback || function () { };

        child.on('error', callback);
        child.on('close', function (code) { callback(code === 0 ? null : new Error(util.format('%s exited with code %d', command, code))); });
    }

    if (process.env.EXEC_LOG_FILE) {
        var logStream = fs.createWriteStream(path.resolve(process.env.EXEC_LOG_FILE), { flags: 'a' });
        logStream.on('open', function () {
            console.log('Using %s for exec logs.', process.env.EXEC_LOG_FILE);

            // add log marker
            logStream.write('\n');
            logStream.write('==================== exec ====================\n');
            logStream.write(util.format('--> %s %s\n', command, args.join(' ')));
            logStream.write('==============================================\n');
            logStream.write('\n');

            options.stdio = [ process.stdin, logStream, logStream ];

            spawnNow();
        });
    } else {
        options.stdio = 'inherit';  // pipe output to console

        spawnNow();
    }
}

function getSSH(host, sshKey, cmd, port, user) {
    cmd = cmd || '';
    cmd = Array.isArray(cmd) ? cmd.join(' ') : cmd;
    port = port || '202';
    user = user || 'root';

    if (sshKey) {
        // try to detect ssh key
        sshKey = findSSHKey(sshKey);
        if (!sshKey) exit('SSH key not found');
    }

    var SSH = '%s@%s -tt -p %s -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o ConnectTimeout=10';
    var out = util.format(SSH, user, host, port);

    // only add this if we have an sshKey
    if (sshKey) out += ' -i ' + sshKey;

    out += ' ' + cmd;

    return out.split(' ');
}

function findSSHKey(key) {
    assert.strictEqual(typeof key, 'string');

    var test = key;

    // remove .pub in case the user passed in the public key
    if (path.extname(test) === '.pub') test = test.slice(0, -4);

    if (fs.existsSync(test)) return test;

    test = path.join(os.homedir(), '.ssh', key);
    if (fs.existsSync(test)) return test;

    test = path.join(os.homedir(), '.ssh', 'id_rsa_' + key);
    if (fs.existsSync(test)) return test;

    test = path.join(os.homedir(), '.ssh', 'id_rsa_caas_' + key);
    if (fs.existsSync(test)) return test;

    return null;
}

function getSSHFingerprint(keyFilePath) {
    assert.strictEqual(typeof keyFilePath, 'string');

    var result = [];

    // We have to try two methods for aws
    // http://blog.jbrowne.com/?p=23#comment-817
    var cmd0 = 'openssl pkey -in ' + keyFilePath + ' -pubout -outform DER | openssl md5 -c';
    var fingerprint0 = safe.child_process.execSync(cmd0);

    var cmd1 = 'openssl pkcs8 -in ' + keyFilePath + ' -nocrypt -topk8 -outform DER | openssl sha1 -c';
    var fingerprint1 = safe.child_process.execSync(cmd1);

    // FIXME again this most likely only works on linux
    var cmd2 = 'ssh-keygen -E md5 -lf ' + keyFilePath;
    var fingerprint2 = safe.child_process.execSync(cmd2);

    // extract colon separated fingerprint from stdoutput
    if (fingerprint0) result.push(fingerprint0.toString('utf-8').slice('(stdin)= '.length).slice(0, -1));
    if (fingerprint1) result.push(fingerprint1.toString('utf-8').slice('(stdin)= '.length).slice(0, -1));
    if (fingerprint2) result.push(fingerprint2.toString('utf-8').slice('(stdin)= '.length).slice(0, -1).split(' ')[0]);

    debug('SSH Key fingerprints', result);

    return result;
}

function getSSHPublicKey(keyFilePath) {
    assert.strictEqual(typeof keyFilePath, 'string');

    var cmd = 'ssh-keygen -y -f ' + keyFilePath;
    var publicKey = safe.child_process.execSync(cmd);
    if (!publicKey) return null;

    return publicKey.toString('utf-8');
}

function createUrl(api) {
    assert.strictEqual(typeof api, 'string');

    if (!config.apiEndpoint()) exit(util.format('Not setup yet. Please use the ' + 'login'.bold + ' command first.'.red));

    // check if this cloudron is set for selfsigned certs
    if (config.allowSelfsigned()) process.env.NODE_TLS_REJECT_UNAUTHORIZED='0';

    return 'https://' + config.apiEndpoint() + api;
}

function authenticate(options, callback) {
    assert.strictEqual(typeof options, 'object');

    if (!options.username && !options.password) {
        console.log();
        console.log('Enter credentials for ' + config.apiEndpoint().bold + ':');
    }

    var username = options.username || readlineSync.question('Username: ', {});
    var password = options.password || readlineSync.question('Password: ', { noEchoBack: true });
    var totpToken = options.totpToken;

    config.setToken(null);

    superagent.post(createUrl('/api/v1/developer/login')).send({
        username: username,
        password: password,
        totpToken: totpToken
    }).end(function (error, result) {
        if (error && !error.response) exit(error);
        if (result.statusCode === 412) {
            showDeveloperModeNotice();
            return authenticate({}, callback);
        }
        if (result.statusCode === 401 && result.body.message.indexOf('totpToken') !== -1) {
            // stash the username and password if only 2fa token is invalid or missing, need a non commander object
            options.username = username;
            options.password = password;

            options.totpToken = readlineSync.question('2FA token: ', {});

            return authenticate(options, callback);
        }
        if (result.statusCode !== 200) {
            console.log('Login failed.'.red);
            return authenticate({}, callback);
        }

        config.setToken(result.body.accessToken || result.body.token); // token was the old api

        console.log('Login successful.'.green);

        if (typeof callback === 'function') callback();
    });
}

// takes a function returning a superagent request instance and will reauthenticate in case the token is invalid
function superagentEnd(requestFactory, options, callback) {
    if (!callback) {
        callback = options;
        options = {};
    }

    requestFactory().end(function (error, result) {
        if (error && error.code == 'DEPTH_ZERO_SELF_SIGNED_CERT') return callback(new Error('To use a Cloudron with a self-signed certificate, run `cloudron login --allow-selfsigned` first'));
        if (error && !error.response) return callback(error);
        if (result.statusCode === 401) return authenticate(options, superagentEnd.bind(null, requestFactory, callback));
        if (result.statusCode === 502) return callback('The Cloudron is currently not responding');

        callback(error, result);
    });
}

function waitForFinish(what, callback) {
    process.stdout.write('Waiting for ' + what + ' to finish...');

    var failCount = 0; // the box might be restarting during updates and nginx will return a 502

    (function checkStatus() {
        superagent.get(createUrl('/api/v1/cloudron/progress')).end(function (error, result) {
            if (error && !error.response) return callback(error);
            if (result.status === 404) return setTimeout(checkStatus, 1000); // try again here
            if (result.statusCode !== 200) {
                if (++failCount > 60) return callback(new Error(util.format('Failed to get ' + what + ' progress.'.red, result.statusCode, result.text)));

                process.stdout.write('x');
                setTimeout(checkStatus, 2000);
                return;
            }

            if (!result.body[what] || result.body[what].percent >= 100) {
                if (result.body[what] && result.body[what].message) return callback(new Error(what + ' failed: ' + result.body[what].message));

                // break line
                console.log('');

                return callback();
            }

            process.stdout.write('.');

            setTimeout(checkStatus, 2000);
        });
    })();
}

function waitForBackupFinish(callback) {
    assert.strictEqual(typeof callback, 'function');

    waitForFinish('backup', callback);
}

function waitForUpdateFinish(callback) {
    assert.strictEqual(typeof callback, 'function');

    async.series([
        waitForFinish.bind(null, 'backup'),
        waitForFinish.bind(null, 'update')
    ], callback);
}

function getDomains(callback) {
    assert.strictEqual(typeof callback, 'function');

    // To support pre 1.9 versions, get the cloudron info first
    detectCloudronApiEndpoint(config.apiEndpoint(), function (error) {
        if (error) return callback(error);

        // old Cloudrons are single domain
        if (semver.lt(config.version(), '1.9.0')) return callback(null, [{ domain: config.cloudronGet('fqdn') }]);

        superagentEnd(function () {
            return superagent.get(createUrl('/api/v1/domains')).query({ access_token: config.token() });
        }, function (error, result) {
            if (error && !error.response) return callback(error);
            if (result.statusCode !== 200) return callback(util.format('Failed to list domains.'.red, result.statusCode, result.text));

            callback(null, result.body.domains);
        });
    });
}

function getCloudronBackupList(callback) {
    assert.strictEqual(typeof callback, 'function');

    superagentEnd(function () {
        return superagent.get(createUrl('/api/v1/backups')).query({ access_token: config.token() });
    }, function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode !== 200) return callback(util.format('Failed to list backups.'.red, result.statusCode, result.text));

        callback(null, result.body.backups);
    });
}

function createCloudronBackup(callback) {
    assert.strictEqual(typeof callback, 'function');

    superagentEnd(function () {
        return superagent
            .post(createUrl('/api/v1/backups'))
            .query({ access_token: config.token() })
            .send({});
    }, function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode === 409) return callback('The Cloudron is unable to backup at the moment. Please retry later.');
        if (result.statusCode !== 202) return callback(util.format('Failed to backup Cloudron.'.red, result.statusCode, result.text));

        waitForBackupFinish(callback);
    });
}

function sshExec(ip, port, sshKey, user, cmds, callback) {
    var sshClient = new SshClient();
    sshClient.connect({
        host: ip,
        port: port || 22,
        username: user || 'root',
        privateKey: fs.readFileSync(sshKey),
        passphrase: process.env.SSH_PASSPHRASE || undefined // hack for now
    });
    sshClient.on('ready', function () {
        console.log('connected');

        async.eachSeries(cmds, function (cmd, iteratorDone) {
            var command = cmd.cmd;

            console.log(command.yellow);

            sshClient.exec(command, function(err, stream) {
                if (err) return callback(err);

                if (cmd.stdin) cmd.stdin.pipe(stream);
                stream.pipe(process.stdout);
                stream.on('close', function () {
                    iteratorDone();
                }).stderr.pipe(process.stderr);
            });
        }, function seriesDone(error) {
            if (error) return callback(error);

            sshClient.end();
        });
    });
    sshClient.on('end', function () {
        console.log('\ndisconnected');
        callback();
    });
    sshClient.on('error', function (error) {
        callback(error);
    });
    sshClient.on('exit', function (exitCode) {
        callback(exitCode === 0 ? null : new Error('ssh exec returned + ' + exitCode));
    });
}

function updateCloudron(callback) {
    assert.strictEqual(typeof callback, 'function');

    superagentEnd(function () {
        return superagent
            .post(createUrl('/api/v1/cloudron/update'))
            .query({ access_token: config.token() })
            .send({});
    }, function (error, result) {
        if (error && !error.response) return callback(error);
        if (result.statusCode === 409) return callback(result.body.message);
        if (result.statusCode === 422) return callback('No update available.');
        if (result.statusCode !== 202) return callback(util.format('Failed to update Cloudron.'.red, result.statusCode, result.text));

        waitForUpdateFinish(callback);
    });
}
