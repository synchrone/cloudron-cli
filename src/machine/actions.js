'use strict';

var assert = require('assert'),
    config = require('../config.js'),
    execSync = require('child_process').execSync,
    fs = require('fs'),
    helper = require('../helper.js'),
    moment = require('moment'),
    os = require('os'),
    path = require('path'),
    superagent = require('superagent'),
    Table = require('easy-table'),
    util = require('util');

exports = module.exports = {
    listBackups: listBackups,
    createBackup: createBackup,
    eventlog: eventlog,
    info: info,
    logs: logs,
    ssh: sshLogin,
    hotfix: hotfix,
    update: update
};

function isEC2() {
    return (config.provider() === 'ec2' || config.provider() === 'lightsail' || config.provider() === 'ami');
}

function defaultSshUser() {
    return (isEC2() || config.provider() === 'gce') ? 'ubuntu' : 'root';
}

function listBackups(options) {
    if (arguments.length > 1) return arguments[arguments.length-1].outputHelp();
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        if (error) helper.exit(error);

        helper.getCloudronBackupList(function (error, result) {
            if (error) helper.exit(error);

            if (result.length === 0) {
                console.log('No backups have been made.');
                helper.exit();
            }

            result = result.map(function (backup) {
                backup.creationTime = new Date(backup.creationTime);
                return backup;
            }).sort(function (a, b) { return b.creationTime - a.creationTime; });

            var t = new Table();

            result.forEach(function (backup) {
                t.cell('Id', backup.id);
                t.cell('Creation Time', backup.creationTime);
                t.cell('Version', backup.version);

                t.newRow();
            });

            console.log('');
            console.log(t.toString());

            helper.exit();
        });
    });
}

function createBackup(options) {
    if (arguments.length > 1) return arguments[arguments.length-1].outputHelp();
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        if (error) helper.exit(error);

        function done(error) {
            if (error) helper.exit(error);
            console.log('Backup successful');
        }

        if (options.sshKey || options.sshPort || options.sshUser) {
            options.sshKey = options.sshKey || config.sshKey();
            options.sshPort = options.sshPort || config.sshPort() || (config.provider() === 'caas' ? 202 : 22);
            options.sshUser = options.sshUser || defaultSshUser();

            helper.exec('ssh', helper.getSSH(config.apiEndpoint(), options.sshKey, ' curl --fail -X POST http://127.0.0.1:3001/api/v1/backup', options.sshPort, options.sshUser), helper.waitForBackupFinish.bind(null, done));
        } else {
            helper.createCloudronBackup(done);
        }
    });
}

function eventlogDetails(eventLog) {
    var data = eventLog.data;
    var errorMessage = data.errorMessage;

    // keep this in sync with eventlog.js and webadmin UI
    var ACTION_ACTIVATE = 'cloudron.activate';
    var ACTION_APP_CONFIGURE = 'app.configure';
    var ACTION_APP_INSTALL = 'app.install';
    var ACTION_APP_RESTORE = 'app.restore';
    var ACTION_APP_UNINSTALL = 'app.uninstall';
    var ACTION_APP_UPDATE = 'app.update';
    var ACTION_APP_LOGIN = 'app.login';
    var ACTION_BACKUP_FINISH = 'backup.finish';
    var ACTION_BACKUP_START = 'backup.start';
    var ACTION_CERTIFICATE_RENEWAL = 'certificate.renew';
    var ACTION_CLI_MODE = 'settings.climode';
    var ACTION_START = 'cloudron.start';
    var ACTION_UPDATE = 'cloudron.update';
    var ACTION_USER_ADD = 'user.add';
    var ACTION_USER_LOGIN = 'user.login';
    var ACTION_USER_REMOVE = 'user.remove';
    var ACTION_USER_UPDATE = 'user.update';

    switch (eventLog.action) {
    case ACTION_ACTIVATE: return 'Cloudron activated';
    case ACTION_APP_CONFIGURE: return 'App ' + data.appId + ' was configured';
    case ACTION_APP_INSTALL: return 'App ' + data.manifest.id + '@' + data.manifest.version + ' installed at ' + data.location; // data.appId
    case ACTION_APP_RESTORE: return 'App ' + data.appId + ' restored';
    case ACTION_APP_UNINSTALL: return 'App ' + data.appId + ' uninstalled';
    case ACTION_APP_UPDATE: return 'App ' + data.appId + ' updated to version ' + data.toManifest.id + '@' + data.toManifest.version;
    case ACTION_APP_LOGIN: return 'App ' + data.appId + ' logged in';
    case ACTION_BACKUP_START: return 'Backup started';
    case ACTION_BACKUP_FINISH: return 'Backup finished'; // + (errorMessage ? ('error: ' + errorMessage) : ('id: ' + data.filename));
    case ACTION_CERTIFICATE_RENEWAL: return 'Certificate renewal for ' + data.domain + (errorMessage ? ' failed' : 'succeeded');
    case ACTION_CLI_MODE: return 'CLI mode was ' + (data.enabled ? 'enabled' : 'disabled');
    case ACTION_START: return 'Cloudron started with version ' + data.version;
    case ACTION_UPDATE: return 'Updating to version ' + data.boxUpdateInfo.version;
    case ACTION_USER_ADD: return 'User ' + data.email + ' added with id ' + data.userId;
    case ACTION_USER_LOGIN: return 'User ' + data.userId + ' logged in';
    case ACTION_USER_REMOVE: return 'User ' + data.userId + ' removed';
    case ACTION_USER_UPDATE: return 'User ' + data.userId + ' updated';
    default: return eventLog.action;
    }
}

function eventlog(options) {
    if (arguments.length > 1) return arguments[arguments.length-1].outputHelp();
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        if (error) helper.exit(error);

        if (options.sshKey || options.sshPort || options.sshUser) {
            options.sshKey = options.sshKey || config.sshKey();
            options.sshPort = options.sshPort || config.sshPort() || (config.provider() === 'caas' ? 202 : 22);
            options.sshUser = options.sshUser || defaultSshUser();

            var cmd;
            if (options.raw) {
                cmd = ' mysql -uroot -ppassword -e "SELECT creationTime,action,source,data FROM box.eventlog ORDER BY creationTime DESC"';
            } else {
                cmd = ' mysql -uroot -ppassword -e "SELECT creationTime,action,source,LEFT(data,50) AS data_preview FROM box.eventlog ORDER BY creationTime DESC"';
            }

            helper.exec('ssh', helper.getSSH(endpoint, options.sshKey, cmd, options.sshPort, options.sshUser));

            return;
        }

        helper.superagentEnd(function () {
            return superagent
                .get(helper.createUrl('/api/v1/cloudron/eventlog'))
                .query({ access_token: config.token() })
                .send({});
        }, function (error, result) {
            if (error) helper.exit(error);
            if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch eventlog.'.red, result.statusCode, result.text));

            var t = new Table();

            result.body.eventlogs.forEach(function (event) {
                t.cell('When', moment(event.creationTime).fromNow());
                t.cell('Action', event.action);
                t.cell('Source', event.source.username || event.source.userId || event.source.ip || event.source.appId || event.source.authType);
                t.cell('Info', options.raw ? JSON.stringify(event.data) : eventlogDetails(event));

                t.newRow();
            });

            console.log();
            console.log(t.toString());

            helper.exit();
        });
    });
}

function info(options) {
    if (arguments.length > 1) return arguments[arguments.length-1].outputHelp();
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        if (error) helper.exit(error);

        var info = null;

        helper.superagentEnd(function () {
            return superagent
                .get(helper.createUrl('/api/v1/config'))
                .query({ access_token: config.token() })
                .send({});
        }, function (error, result) {
            if (error) helper.exit(error);
            if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch info.'.red, result.statusCode, result.text));

            info = result.body;

            helper.superagentEnd(function () {
                return superagent
                    .get(helper.createUrl('/api/v1/settings/backup_config'))
                    .query({ access_token: config.token() })
                    .send({});
            }, function (error, result) {
                if (error) helper.exit(error);
                if (result.statusCode !== 200) return helper.exit(util.format('Failed to fetch backup info.'.red, result.statusCode, result.text));

                info.backupConfig = result.body;

                if (options.raw) {
                    console.log(info);
                } else {
                    console.log('');
                    console.log('Cloudron %s info:', config.apiEndpoint().yellow);
                    console.log('');
                    console.log(' Appstore:     %s', info.apiServerOrigin.cyan);
                    console.log(' Version:      %s', info.version.cyan);
                    console.log(' Provider:     %s', info.provider.cyan);
                    console.log('');

                    console.log('Backup config:');
                    console.log('');
                    Object.keys(info.backupConfig).forEach(function (key) {
                        var prettyKey = key;
                        var prettyValue = info.backupConfig[key] ? info.backupConfig[key].cyan : 'unset'.gray;

                        if (key === 'key') prettyKey = 'Encryption Key';

                        console.log(' %s: %s %s', prettyKey, new Array(20 - prettyKey.length).join(' '), prettyValue);
                    });
                    console.log('');
                }
            });
        });
    });
}

function logs(options) {
    if (arguments.length > 1) return arguments[arguments.length-1].outputHelp();
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        var ip = null;

        if (error) {
            if (helper.isIp(endpoint)) {
                ip = endpoint;
            } else {
                helper.exit(error);
            }
        }

        if (!helper.isIp(endpoint)) {
            options.sshKey = options.sshKey || config.sshKey();
            options.sshPort = options.sshPort || config.sshPort() || (config.provider() === 'caas' ? 202 : 22);
            options.sshUser = options.sshUser || defaultSshUser();
        }

        var journalctl = (defaultSshUser() !== 'root' ? 'sudo ' : '') + 'journalctl -a' + (options.follow ? ' -f' : '');
        helper.exec('ssh', helper.getSSH(ip || config.apiEndpoint(), options.sshKey, journalctl, options.sshPort, options.sshUser), function (error) {
            helper.exit(error);
        });
    });
}

function sshLogin(cmds, options) {
    assert(Array.isArray(cmds));
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        var ip = null;

        if (error) {
            if (helper.isIp(endpoint)) {
                ip = endpoint;
            } else {
                helper.exit(error);
            }
        }

        if (!helper.isIp(endpoint)) {
            options.sshKey = options.sshKey || config.sshKey();
            options.sshPort = options.sshPort || config.sshPort() || (config.provider() === 'caas' ? 202 : 22);
            options.sshUser = options.sshUser || defaultSshUser();
        }

        helper.exec('ssh', helper.getSSH(ip || config.apiEndpoint(), options.sshKey, cmds, options.sshPort, options.sshUser), function (error) {
            if (error) helper.exit(error);

            // preserve option if no ip was passed
            if (!helper.isIp(endpoint)) {
                config.setSshKey(options.sshKey);
                config.setSshPort(options.sshPort);
            }
        });
    });
}

function hotfix(options) {
    if (arguments.length > 1) return arguments[arguments.length-1].outputHelp();
    assert.strictEqual(typeof options, 'object');

    if (!options.cloudron) helper.missing('cloudron');

    var endpoint = options.cloudron;

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        var ip = null;
        var code = null;

        if (error) {
            if (helper.isIp(endpoint)) {
                ip = endpoint;
            } else {
                helper.exit(error);
            }
        }

        if (!helper.isIp(endpoint)) {
            options.sshKey = options.sshKey || config.sshKey();
            options.sshPort = options.sshPort || config.sshPort() || (config.provider() === 'caas' ? 202 : 22);
            options.sshUser = options.sshUser || defaultSshUser();
        }

        if (!options.sshKey) helper.missing('ssh-key');

        options.sshKey = helper.findSSHKey(options.sshKey);
        if (!options.sshKey) helper.exit('Unable to find SSH key');

        // required since we mix async and sync code paths
        function doHotfix() {
            var cmds = [
                { cmd: 'sudo rm -rf /tmp/box-src-hotfix /tmp/installer_data.json' },
                { cmd: 'sudo mkdir -p /tmp/box-src-hotfix' },
                { cmd: 'sudo tar zxf - -C /tmp/box-src-hotfix', stdin: fs.createReadStream(code) },
                { cmd: 'sudo tee /tmp/remote_hotfix.js', stdin: fs.createReadStream(path.resolve(__dirname, './remote_hotfix.js')) },
                { cmd: 'sudo HOME=/home/yellowtent BOX_ENV=cloudron node /tmp/remote_hotfix.js' },
                { cmd: 'sudo rm -rf /tmp/box-src-hotfix /tmp/installer_data.json' }
            ];

            helper.sshExec(ip || config.apiEndpoint(), options.sshPort, options.sshKey, options.sshUser, cmds, function (error) {
                if (error) helper.exit(error);

                // preserve option if no ip was passed
                if (!helper.isIp(endpoint)) {
                    config.setSshKey(options.sshKey);
                    config.setSshPort(options.sshPort);
                }

                console.log('Done patching'.green);
            });
        }

        code = os.tmpdir() + '/boxtarball.tar.gz';
        var tarballScript = path.join(process.cwd(), 'scripts/createReleaseTarball');
        if (!fs.existsSync(tarballScript)) helper.exit('Could not find createReleaseTarball script. Run this command from the box repo checkout directory.');

        try {
            execSync(tarballScript + ' --output ' + code, { stdio: [ null, process.stdout, process.stderr ] });
        } catch (e) {
            console.log('Unable to create version tarball.'.red);
            process.exit(1);
        }

        doHotfix();
    });
}

function update(options) {
    assert.strictEqual(typeof options, 'object');

    var endpoint = options.cloudron || config.apiEndpoint();

    helper.detectCloudronApiEndpoint(endpoint, function (error) {
        if (error) helper.exit(error);

        function done(error) {
            if (error) helper.exit(error);
            console.log('Update successful');
        }

        if (options.sshKey || options.sshPort || options.sshUser) {
            options.sshKey = options.sshKey || config.sshKey();
            options.sshPort = options.sshPort || config.sshPort() || (config.provider() === 'caas' ? 202 : 22);
            options.sshUser = options.sshUser || defaultSshUser();

            helper.exec('ssh', helper.getSSH(config.apiEndpoint(), options.sshKey, ' curl --fail -X POST http://127.0.0.1:3001/api/v1/update', options.sshPort, options.sshUser), function (error) {
                if (error) return helper.exit('Failed to update');

                helper.waitForUpdateFinish(done);
            });
        } else {
            helper.updateCloudron(done);
        }
    });
}
