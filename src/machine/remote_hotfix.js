'use strict';

if (process.env.BOX_ENV !== 'cloudron') {
    console.error('!! This is only meant to be run with cloudron hotfix');
    process.exit(1);
}

var spawn = require('child_process').spawn,
    fs = require('fs'),
    path = require('path');

// source config from old box code HOME has to be /home/yellowtent
var config = require('/home/yellowtent/box/src/config.js');

var NEW_BOX_SOURCE_DIR = '/tmp/box-src-hotfix';
var INSTALLER_DATA_FILE = '/tmp/installer_data.json';

// THIS has to be the same as in cloudron.js
var data= {
    provider: config.provider(),
    apiServerOrigin: config.apiServerOrigin(),
    webServerOrigin: config.webServerOrigin(),
    adminDomain: typeof config.adminDomain === 'function' ? config.adminDomain() : config.fqdn(),
    adminLocation: config.adminLocation(),
    adminFqdn: typeof config.adminFqdn === 'function' ? config.adminFqdn() : config.adminLocation() + (config.isCustomDomain() ? '.' : '-') + config.adminDomain(),
    isDemo: config.isDemo(),

    appstore: {
        apiServerOrigin: config.apiServerOrigin()
    },
    caas: {
        apiServerOrigin: config.apiServerOrigin(),
        webServerOrigin: config.webServerOrigin()
    },

    version: config.version(),
    edition: config.edition()
};

console.log('=> Preparing data for installer.sh');
console.log(data);

// let things crash if it fails
fs.writeFileSync(INSTALLER_DATA_FILE, JSON.stringify(data));

console.log('=> Running installer.sh');
var installer = spawn(path.join(NEW_BOX_SOURCE_DIR, 'scripts/installer.sh'), [ '--data-file', INSTALLER_DATA_FILE ]);

installer.stdout.pipe(process.stdout);
installer.stderr.pipe(process.stderr);

installer.on('exit', function (code) {
    console.log('Finished with code', code);
    process.exit(code);
});
