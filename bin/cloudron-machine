#!/usr/bin/env node

'use strict';

require('supererror');
require('colors');

var program = require('commander'),
    actions = require('../src/machine/actions.js');

program.version(require('../package.json').version);

program.command('backup', 'Manage Cloudron backups'); // this has subcommands. see cloudron-machine-backup

program.command('eventlog')
    .description('Get Cloudron eventlog')
    .option('--cloudron <domain/ip>', 'Cloudron domain or IP')
    .option('--raw', 'Fetch full eventlog')
    .option('--ssh-port <ssh port>', 'SSH port')
    .option('--ssh-key <ssh key>', 'SSH key')
    .option('--ssh-user <ssh user>', 'SSH username')
    .action(actions.eventlog);

program.command('hotfix')
    .description('Hotfix Cloudron with latest code')
    .option('--cloudron <domain/ip>', 'Cloudron domain or IP')
    .option('--ssh-port <ssh port>', 'SSH port')
    .option('--ssh-key <ssh key>', 'SSH Key file path')
    .option('--ssh-user <ssh user>', 'SSH username')
    .action(actions.hotfix);

program.command('info')
    .description('Get Cloudron info')
    .option('--cloudron <domain>', 'Cloudron domain')
    .option('--raw', 'Dump raw JSON')
    .action(actions.info);

program.command('logs')
    .description('Get Cloudron logs')
    .option('--cloudron <domain/ip>', 'Cloudron domain or IP')
    .option('-f, --follow', 'Follow')
    .option('--ssh-port <ssh port>', 'SSH port')
    .option('--ssh-key <ssh key>', 'SSH key')
    .option('--ssh-user <ssh user>', 'SSH username')
    .action(actions.logs);

program.command('ssh [cmds...]')
    .description('Get a remote SSH connection')
    .option('--cloudron <domain/ip>', 'Cloudron domain or IP')
    .option('--ssh-port <ssh port>', 'SSH port')
    .option('--ssh-key <ssh key>', 'SSH key')
    .option('--ssh-user <ssh user>', 'SSH username')
    .action(actions.ssh);

program.command('update')
    .description('Update Cloudron')
    .option('--cloudron <domain/ip>', 'Cloudron domain or IP')
    .option('--ssh-port <ssh port>', 'SSH port')
    .option('--ssh-key <ssh key>', 'SSH key')
    .option('--ssh-user <ssh user>', 'SSH username')
    .action(actions.update);

if (!process.argv.slice(2).length) {
    program.outputHelp();
} else { // https://github.com/tj/commander.js/issues/338
    // deal first with global flags!
    program.parse(process.argv);

    if (process.argv[2] === 'help') {
        return program.outputHelp();
    }

    var knownCommand = program.commands.some(function (command) { return command._name === process.argv[2] || command._alias === process.argv[2]; });
    if (!knownCommand) {
        console.log('Unknown command: ' + process.argv[2].bold + '.\nTry ' + 'cloudron machine help'.yellow);
        process.exit(1);
    }
    return;
}

program.parse(process.argv);
