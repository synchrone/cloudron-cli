#!/usr/bin/env node

/* global it:false */
/* global describe:false */

'use strict';

var child_process = require('child_process'),
    expect = require('expect.js'),
    path = require('path'),
    util = require('util');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

if (!process.env.CLOUDRON) return console.error('CLOUDRON env var not set');
if (!process.env.USERNAME) return console.error('USERNAME env var not set');
if (!process.env.PASSWORD) return console.error('PASSWORD env var not set');
if (!process.env.SSH_KEY) return console.error('SSH_KEY env var not set');

var cloudron = process.env.CLOUDRON;
var username = process.env.USERNAME;
var password = process.env.PASSWORD;
var sshKey = process.env.SSH_KEY;

var CLI = path.resolve(__dirname + '/../bin/cloudron');

function cli(args, options) {
    // https://github.com/nodejs/node-v0.x-archive/issues/9265
    options = options || { };
    args = util.isArray(args) ? args : args.match(/[^\s"]+|"([^"]+)"/g);
    args = args.map(function (e) { return e[0] === '"' ? e.slice(1, -1) : e; }); // remove the quotes

    try {
        var cp = child_process.spawnSync(CLI, args, { stdio: [ options.stdin || 'pipe', options.stdout || 'pipe', 'pipe' ], encoding: options.encoding || 'utf8' });
        return cp;
    } catch (e) {
        console.error(e);
        throw e;
    }
}

// can't use before() since it will be run prior to other test files executing and cloudron-test.js does a logout at the end
describe('Setup', function () {
    it('login', function (done) {
        cli(util.format('login %s --username %s --password %s', cloudron, username, password));
        done();
    });
});

describe('Backup', function () {
    it('can create with rest route', function (done) {
        var out = cli('machine backup create');
        expect(out.stdout).to.contain('Backup successful');
        done();
    });

    it('can create using ssh', function (done) {
        var out = cli(util.format('machine backup create --cloudron %s --ssh-key %s', cloudron, sshKey));
        expect(out.stdout).to.contain('Backup successful');
        done();
    });

    it('can list', function (done) {
        var out = cli('machine backup list');
        var backupCount = out.stdout.split('\n').filter(function(l) { return l.match(/box_/); }).length;
        expect(backupCount).to.be.greaterThan(sshKey ? 1 : 0);
        done();
    });
});

describe('Eventlog', function () {
    it('succeeds with rest route', function (done) {
        var out = cli(util.format('machine eventlog'));
        // we can test for backup finished as we just did a backup
        expect(out.stdout).to.contain('Backup finished');
        done();
    });

    it('succeeds with ssh', function (done) {
        var out = cli(util.format('machine eventlog --cloudron %s --ssh-key %s', cloudron, sshKey));
        expect(out.stdout).to.contain('backup.finish');
        done();
    });
});

// FIXME not sure how to test this, since it is a continous stream
// if (sshKey) {
//     describe('Cloudron logs', function () {
//         it('succeeds', function (done) {
//             var out = cli(util.format('logs %s --ssh-key %s', cloudron, sshKey));
//             expect(out.stdout.indexOf('creationTime')).to.not.be(-1);
//             done();
//         });
//     });
// }

describe('ssh', function () {
    it('succeeds', function (done) {
        var out = cli(util.format('machine ssh --ssh-key %s echo foobar', sshKey));
        expect(out.stdout).to.contain('foobar');
        done();
    });

    it('succeeds explicitly', function (done) {
        var out = cli(util.format('machine ssh --cloudron %s echo foobar --ssh-key %s', cloudron, sshKey));
        expect(out.stdout).to.contain('foobar');
        done();
    });
});
